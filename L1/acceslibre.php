
<h3>La salle d'accès libre</h3>

<p>
Une salle informatique en accès libre est ouverte aux étudiants de 
1ère année de Licence pour leur permettre de travailler les 
TP d'informatique. La salle est :
</p>

<ul>
  <li>située au bâtiment SUP, 1er étage, salle SUP 115</li>
  <li>accessible du lundi au vendredi sur présentation de la carte d'étudiant, de début octobre à fin mai</li>
</ul>


<h3>Le tutorat</h3>

<p>
Des séances de tutorat sont proposées aux étudiant.e.s en difficulté et
voulant être accompagné.e.s dans l'apprentissage des UE d'informatique.
Les séances se déroulent :
</p>

<ul>
  <li>4 fois par semaine à partir de la 3ème semaine des enseignements des S1 et S2
  (les jours et horaires varient chaque semestre en fonction des
  contraintes des encadrant·es)</li>

  <li>en salle informatique : au bâtiment SUP, 1er étage, dans une des salles SUP-116 à SUP-120</li>

  <li>encadrées par des étudiants en 1ère année de Master Informatique</li>
</ul>
  
<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   

