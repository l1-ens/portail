<h3>Responsable</h3>

<ul>
  <li><a href="mailto:maude.pupin@univ-lille.fr">Maude PUPIN</a> -
Coordinatrice des enseignements d'informatique de la première année de Licence
  </li>

</ul>

<h3>Directeurs et directrices des études</h3>

<ul>
  <li>MIASHS : <a href="mailto:abdellah.hanani@math.univ-lille.fr">Abdellah HANANI</a> -
directeur des études
  </li>

  <li>PEIP : <a href="mailto:xavier.melique@iemn.univ-lille.fr">Xavier MELIQUE</a> -
directeur des études
  </li>

  <li>SESI : <a href="mailto:fanny.minvielle@univ-lille.fr">Fanny Minvielle</a> -
directrice des études
  </li>

  <li>Parcours aménagé SESI : <a href="mailto:fabien.waquet@univ-lille.fr">Fabien Waquet</a> - directeur des études
  </li>

</ul>

<h3>Secrétariats pédagogiques</h3>

<ul>
  <li>MIASHS : <a href="mailto:Amandine.Baisson@univ-lille.fr">Amandine Baisson</a>,
  Bât M2, porte 12, tel. 03 20 43 42 39 </li>
  <li>PEIP : <a href="mailto:Dinah.Brusel@univ-lille1.fr">Dinah Brusel</a>,
  Bât SUP, porte 05, tel. 03 20 05 87 27 </li>
  <li>SESI : <a href="mailto:carine.colpaert@univ-lille.fr">Carine Colpaert</a>
  et <a href="mailto:soizick.tribovillard@univ-lille.fr">Soizick Tribovillard</a>,
  Bât SUP, porte 01, tel. 03 20 05 87 28 et 03 62 26 86 49</li>
  <li>Parcours aménagé SESI : <a href="mailto:amenage-SESI@univ-lille.fr">Véronique Vyncke</a>,
  Bât SUP, porte 114, Tél. 03 20 05 86 50 </li>
</ul>


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   
