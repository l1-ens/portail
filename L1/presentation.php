
<h3>La première année de Licence</h3>


<p>
Les enseignements d'informatique de la première année de licence 
sont gérés par le département informatique de la Faculté des Sciences 
et Technologies de Lille.
Les informations concernant les UE des premier et second semestres 
sont accessibles via les onglets L1S1 et L1S2.<br/>
Ces enseignements concernent les étudiants de plusieurs portails 
et formations.
</p>

<ul>
  <li> <b>MIASHS :</b> Mathématiques et Informatique Appliquées 
  aux Sciences Humaines et Sociales </li>
  <li> <b>PEIP :</b> Parcours des Écoles d'Ingénieurs Polytech'Lille 
  (prépa intégrée) </li>
  <li> <b>SESI :</b> Sciences Exactes et Sciences de l'Ingénierie </li>
  <li> <b>Parcours aménagé SESI :</b> à destination des titulaires de baccalauréats technologiques</li>
</ul>

<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   
