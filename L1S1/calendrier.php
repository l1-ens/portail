<h3>Début des enseignements d'informatique</h3>

<p>Semaine du 12 septembre</p>

<h3>Fin des enseignements</h3>

<p>Semaine du 12 décembre</p>

<h3>Devoirs Surveillés</h3>

<p>Les DS sont communs à toutes les formations et sont organisés 
lors des semaines banalisées :</p>

<ul>
  <li>DS intermédiaire (DSi) : semaine du 7 novembre</li>
  <li>DS final (DSf) : entre le 3 et le 13 janvier</li>
  <li>DS rattrapage (DSr) : entre le 5 et le 16 juin</li>
</ul>


<h3>Interruptions pédagogiques</h3>

<ul>
  <li>du lundi 31 octobre au samedi 5 novembre</li>
  <li>du samedi 17 décembre au lundi 2 janvier (inclus)</li>
</ul>

<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   
