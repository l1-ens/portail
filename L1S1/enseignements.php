<h2>Les UE d'informatique en Licences SESI/PEIP/MIASHS au semestre 1 </h2>

<dl>
  <dt>UE informatique</dt>
  <dd>Introduction à la programmation et à l'algorithmique (6 ECTS). 
  En SESI, cette UE fait partie des 3 UE à choisir parmi 4. 
  EN MIASHS et PEIP, cette UE est obligatoire</dd>
</dl>

<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   
