<div class="semainier">        


<table class="table">

<tr class="entete">
<th>  </th>
<th> Cours </th>
<th> TD ou TP</th>
<th> Remarque </th>
</tr>



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 17/01 au 21/01</td>
<td> 
<!-- COURS -->
Représentation des nombres. 
</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 24/01 au 28/01</td>
<td> 
<!-- COURS -->
Algorithmes de calcul. Bases, changement de bases, binaire.  
</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<!-- FIN SEANCE -->
<td>du 31/01 au 04/02</td>
<td>
<!-- COURS -->
</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- FIN SEANCE -->
<tr class="seance" >
<td>du 7/02 au 11/02</td>
<td>
<!-- COURS -->
Représentation des réels. 
</td>
<td>
<!-- TD -->

</td>

<td class="remarque"> 

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<td>du 14/02 au 18/02</td>
<td>
</td>
<td> 
</td>
<td class="remarque">
<!-- REMARQUE -->
interruption pédagogique hiver
</td>
</tr>
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 21/02 au 25/02</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 28/02 au 04/03</td>
<td> 
<!-- COURS -->
Codage des caractères, autres codages divers. 
</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 07/03 au 11/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 14/03 au 18/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="seance" >
<td>du 21/03 au 25/03</td>
<td> <!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 28/03 au 01/04</td>
<td> 
</td>
<td> 
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 4/04 au 8/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 11/04 au 15/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->
Interruption pédagogique de printemps
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="libre" >
<td>du 18/04 au 22/04</td>
<td> </td>
<td> </td>
<td class="remarque"> 
<!-- REMARQUE -->
interruption pédagogique de printemps
</td>
</tr>
<!-- ========================================================= -->




<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 02/05 au 06/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>

<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE  -->
<!-- ========================================================= -->



</table>



</div>


