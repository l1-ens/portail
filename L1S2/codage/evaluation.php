<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<p>
L'évaluation s'effectue suivant une procédure de contrôle continu. 

Deux notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>
<li> <span class="NOTE">DS1</span> : une note sur 20 d'un devoir
surveillé en milieu de semestre.</li> 
<li> <span class="NOTE">DS2</span> : une note sur 20 d'un devoir
surveillé en fin de semestre.</li> 
</ul>




<p>
La note finale sur 20 (<span class="NOTE">N</span>) est calculée comme
une moyenne de ces notes : 
</p>

<p class="FORMULE">
 N = (DS1+DS2) /2
</p>



<p>La session de rattrapage remplace la partie <i>sup(DS1 + 2*DS2, 3*DS2)</i>.  
</p>
 

<p>
L'unité acquise apporte 3 ECTS.
</p>


