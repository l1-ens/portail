<!-- objectifs de l'UE -->
<h3> Objectifs </h3>


<!-- le contenu -->
<h3> Contenu </h3>
<ul>
<li> Représentation des nombres. </li>
<li> Algorithmes arithmétiques (addition, multiplication,...)</li>
<li> Binaire, octal, hexadécimal : usages en informatique</li>
<li> Représentation des nombres réels </li>
<li> Codage des caractères et des informations hétérogènes</li>
</ul>

<!-- bibliographie accompagnant l'UE -->
<h3> Bibliographie </h3>
<ul>
<li> Histoire universelle des chiffres, Georges Ifrah </li>
</ul>

