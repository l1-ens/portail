<h3>Les UE d'informatique en Licences SESI/PEIP/MIASHS au semestre 2</h3>

<dl>
  <dt>EC AP</dt>
  <dd>Algorithmes et Programmation, prérequis pour la licence informatique,
  enseignement obligatoire dans le parcours maths-info de SESI, 
  en SESI parcours aménagé, en PEIP et en MIASHS</dd>

  <dt>EC TW</dt>
  <dd>Technologies du Web, prérequis pour la licence informatique,
  enseignement obligatoire dans le parcours maths-info de SESI
  et en MIASHS, optionnel en PEIP</dd>

  <dt>UE PE Codage</dt>
  <dd>UE Projet de l'Étudiant Codage, pré-recquis pour la licence informatique,
  peut être choisie par les étudiants du parcours math-info de SESI
  et de MIASHS.</dd>
  
  <dt>UE PE AetC</dt>
  <dd>UE Projet de l'Étudiant Arithmétique et Cryptographie, peut être 
  choisie par les étudiants du parcours math-info de SESI qui ont déjà
  les connaissances en codage. </dd>
  
  <dt>UE PE renforcé-recherche informatique</dt>
  <dd>Cette UE est obligatoire pour les étudiants du parcours
  renforcé-recherche informatique. Elle regroupe du codage et
  de l'arithmétique et cryptographie.</dd>
  
</dl>


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/portail/-/raw/master/signature.php");
?>   
